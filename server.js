var ldapRTApi = require('./ldapRT');
ldapRTApi.init("ldaps://dirserv.reutlingen-university.de:636");


var user_name = "ldap_user";
var password = "ldap_password";

console.log("Send user login request please wait...")
ldapRTApi.isVaildUser(user_name, password, function (err, data, loginUserHash) {
    if (err) {
        return console.log("Something went wrong:", err)
    } else if (data && data.mail) { //Check auf mitarbeiter
        if(data.mail.toLowerCase().endsWith("@reutlingen-university.de")) {
            data["isMitarbeiter"] = true;
        }
        console.log("success", data);
    } else {
        console.log("Insufficient permissions!", data)
    }
})
