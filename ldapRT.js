//@ts-check
var ldap = require('ldapjs');
var async = require("async");
var crypto = require('crypto');
var ldapURL = "";

var init = function (nldapURL = "") {
    ldapURL = nldapURL;
}

var vaildUserBuffer = {}
var isVaildUser = function (username, password, callback) {
    getUserData(username, password, function (err, data) {
        if (err) {
            callback("Wrong username or password!");
        }
        vaildUserBuffer[username] = crypto.createHash('sha256').update(password).digest('base64');
        callback(false, data, vaildUserBuffer[username])
    });
}

var isActiveHash = function (hash) {
    for (var i in vaildUserBuffer) {
        if (vaildUserBuffer[i] === hash) {
            return true;
        }
    }
    return false;
}

var getAllLdapUserGroups = function (username, ldapPw, onlyTestAuth, callback) { //Returns userdata
    var client = ldap.createClient({
        url: ldapURL
    });

    var userGroups = [];
    var success = true;
    //LDAP Access
    client.bind('uid=' + username + ',ou=people,o=hochschule-reutlingen.de', ldapPw, function (err) {
        if (err) {
            callback("Authentification failed! Wrong User / PW?!", []);
            return console.log("Authentification failed! Wrong User / PW?!");
        }
        if (onlyTestAuth) {
            callback(null, []); //Success
            return;
        }

        var opts = {
            filter: '(&(cn=*)(uniqueMember=uid=' + username + ',ou=People,o=Hochschule-Reutlingen.DE))', //Get all groups from this user
            scope: 'sub'
        };

        client.search('ou=Groups,o=hochschule-reutlingen.de', opts, function (err, res) {
            if (err) {
                success = false;
                callback(err);
                return console.log(err);
            }

            res.on('searchEntry', function (entry) { //On entry
                var gName = entry.objectName || "";
                gName = gName.split(",ou=Groups")[0];
                gName = gName.replace("cn=", "");
                if (gName != "") {
                    userGroups.push(gName)
                }
            });

            res.on('searchReference', function (referral) {
                console.log('referral: ' + referral.uris.join());
            });
            res.on('error', function (err) {
                console.error('error: ' + err.message);
                success = false;
                callback(err.message, []); //fail
            });
            res.on('end', function (result) {
                client.unbind(function (err) {
                    if (err)
                        console.log(err);
                });
                if (success) {
                    callback(null, userGroups); //success
                }
            });
        });
    });
}
var getUserData = function (username, ldapPw, callback) { //Returns userdata
    var client = ldap.createClient({
        url: ldapURL
    });

    var user = null;
    var success = true;
    //LDAP Access
    client.bind('uid=' + username + ',ou=people,o=hochschule-reutlingen.de', ldapPw, function (err) {
        if (err) {
            callback("Authentification failed! Wrong User / PW?!", []);
            return console.log("Authentification failed! Wrong User / PW?!");
        }

        var opts = {
            filter: 'uid=' + username, //Get userdata
            scope: 'sub',
            attributes: ['uid', 'mail', 'givenName', 'sn']
        };

        client.search('ou=People,o=hochschule-reutlingen.de', opts, function (err, res) {
            if (err) {
                success = false;
                callback(err, []);
                return console.log(err);
            }

            res.on('searchEntry', function (entry) { //On entry
                user = entry.object;
            });

            res.on('searchReference', function (referral) {
                console.log('referral: ' + referral.uris.join());
            });
            res.on('error', function (err) {
                console.error('error: ' + err.message);
                success = false;
                callback(err.message, []);
            });
            res.on('end', function (result) {
                client.unbind(function (err) {
                    if (err)
                        console.log(err);
                });
                if (success) {
                    callback(null, user); //Success
                }
            });
        });
    });
}
var getAllMails = function (username, ldapPw, callback) { //Returns all emails
    var client = ldap.createClient({
        url: ldapURL
    });

    var user = null;
    var success = true;
    //LDAP Access
    client.bind('uid=' + username + ',ou=people,o=hochschule-reutlingen.de', ldapPw, function (err) {
        if (err) {
            callback("Authentification failed! Wrong User / PW?!", []);
            return console.log("Authentification failed! Wrong User / PW?!");
        }

        var opts = {
            filter: 'uid=*', //Get userdata
            scope: 'sub',
            attributes: ['uid', 'mail']
        };

        var userMails = [];
        var userObj = {};
        client.search('ou=People,o=hochschule-reutlingen.de', opts, function (err, res) {
            if (err) {
                success = false;
                callback(err, []);
                return console.log(err);
            }

            res.on('searchEntry', function (entry) { //On entry
                //console.log(entry.object)
                if (entry && entry.object && entry.object.mail) {
                    userMails.push(entry.object.mail.toLowerCase());
                    userObj[entry.object.mail.toLowerCase()] = entry.object.uid;
                }
            });

            res.on('searchReference', function (referral) {
                console.log('referral: ' + referral.uris.join());
            });
            res.on('error', function (err) {
                console.error('error: ' + err.message);
                success = false;
                callback(err.message, []);
            });
            res.on('end', function (result) {
                client.unbind(function (err) {
                    if (err)
                        console.log(err);
                });
                if (success) {
                    callback(null, userMails, userObj); //Success
                }
            });
        });
    });
}
//Holt erst alle Mitglieder der Gruppen die eine ".1." im Namen haben, dann hole die Emails der benutzer
var getAllFirstSemesterGroups = function (username, ldapPw, callback) {
    var client = ldap.createClient({
        url: ldapURL
    });

    var allGroups = [];
    var success = true;
    //LDAP Access
    client.bind('uid=' + username + ',ou=people,o=hochschule-reutlingen.de', ldapPw, function (err) {
        if (err) return callback("Authentification failed! Wrong User / PW?!", []);

        var opts = {
            filter: '(&(cn=*.1.*))', //Liefert alle gruppen mit .1. zurück (Alle Erstsemestergruppen)
            scope: 'sub'
        };

        client.search('ou=Groups,o=hochschule-reutlingen.de', opts, function (err, res) {
            if (err) {
                success = false;
                return callback(err);
            }

            res.on('searchEntry', function (entry) { //On entry
                var gName = entry.objectName || "";
                gName = gName.split(",ou=Groups")[0];
                gName = gName.replace("cn=", "");
                if (gName != "") {
                    allGroups.push(gName)
                }
            });

            res.on('searchReference', function (referral) {
                console.log('referral: ' + referral.uris.join());
            });
            res.on('error', function (err) {
                success = false;
                callback(err.message, []); //fail
            });
            res.on('end', function (result) {
                var groupsWithMembers = {};
                var asynArray = [];
                for (var i = 0; i < allGroups.length; i++) {
                    (function () {
                        var groupName = allGroups[i];
                        groupsWithMembers[groupName] = {
                            uids: [],
                            emails: []
                        };
                        asynArray.push(function (acallback) {
                            var opts = {
                                filter: '(cn=' + groupName + ')', //Liefert all Gruppenmitglieder
                                scope: 'sub',
                                attributes: ["uniqueMember"]
                            };

                            client.search('ou=Groups,o=hochschule-reutlingen.de', opts, function (err, res) {
                                if (err) return acallback(err);

                                res.on('searchEntry', function (entry) { //On entry
                                    if (entry && entry.object && entry.object.uniqueMember)
                                        groupsWithMembers[groupName].uids = entry.object.uniqueMember;
                                });

                                res.on('end', function (result) {
                                    var asynArray2 = [];
                                    for (var k = 0; k < groupsWithMembers[groupName].uids.length; k++) {
                                        (function () {
                                            var uid = groupsWithMembers[groupName].uids[k].split(",")[0];
                                            asynArray2.push(function (acallback) {
                                                var opts = {
                                                    filter: uid, //Liefert den Eintrag zu einer bestimmten uid
                                                    scope: 'sub',
                                                    attributes: ["uid", "mail"]
                                                };
                                                client.search('ou=people,o=hochschule-reutlingen.de', opts, function (err, res) {
                                                    if (err) return acallback(err);

                                                    res.on('searchEntry', function (entry) { //On entry
                                                        if (entry && entry.object && entry.object.mail)
                                                            groupsWithMembers[groupName].emails.push(entry.object.mail)
                                                        //console.log(entry.object)
                                                    });

                                                    res.on('end', function (result) {
                                                        acallback(err, res);
                                                    });
                                                });


                                            });


                                        })();
                                    }
                                    async.parallelLimit(asynArray2, 1, function (err, results) {
                                        acallback(err, result);
                                    });

                                });

                                res.on('error', function (err) {
                                    acallback(err, result);
                                });
                            });
                        })
                    })();
                }
                async.parallelLimit(asynArray, 1, function (err, results) {
                    client.unbind(function (err) {
                        if (err)
                            console.log(err);
                    });
                    if (success) {
                        callback(err, groupsWithMembers); //success
                    }
                });
            });
        });
    });
}

module.exports = {
    init: init,
    getAllFirstSemesterGroups: getAllFirstSemesterGroups,
    getUserData: getUserData,
    getAllLdapUserGroups: getAllLdapUserGroups,
    getAllMails: getAllMails,
    isVaildUser: isVaildUser,
    isActiveHash: isActiveHash
}